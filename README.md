ProtoApp | Identify user's bureau de vote
=========================================

Nothing here yet. Talks to the API behind [Interroger sa situation électorale @ service-public.fr](https://www.service-public.fr/particuliers/vosdroits/services-en-ligne-et-formulaires/ISE).
